﻿using System.Collections.Generic;
public class Kata
{
    public static int[] MoveZeroes(int[] arr)
    {
        List<int> result = new List<int>();
        int zeroesCount = 0;
        void AddZeroes(int zeroesCount)
        {
            while (zeroesCount > 0)
            {
                result.Add(0);
                zeroesCount--;
            }
        }
        foreach (int number in arr)
        {
            if (number == 0)
            {
                zeroesCount++;
                continue;
            }
            else
            {
                result.Add(number);
            }
        }
        AddZeroes(zeroesCount);
        return result.ToArray();
    }
}